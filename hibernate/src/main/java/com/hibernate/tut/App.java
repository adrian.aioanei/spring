package com.hibernate.tut;

import java.time.LocalDate;

import org.apache.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	final static Logger logger = Logger.getLogger(App.class);

	public static void main( String[] args )
	{    	
		for (LocalDate date = LocalDate.now(); date.isBefore(LocalDate.now().plusDays(3)); date = date.plusDays(1))
		{
			Boolean flag = true;
			while(flag) {
				if(LocalDate.now().isEqual(date)) {
					logger.error("This is error : " + args.toString());
				}
				try {
					Thread.sleep(180000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}



	}
}
