package com.hibernate.models;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*  
 *  Test some new class
 */ 
public class TestSystem {

	public static void main(String[] args) {

		
		int[] data = {1,2,34};
		Object obj = new Object();
		System.out.println(data.length);
		
		List<Integer> x = new ArrayList<Integer>();
		x.add(1);
		x.add(2);
		x.add(3);
		
		System.out.println(x.size());
		System.out.println();

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();

//		for(int  i = 0;i<10;i++) {
//			UserDetails user = new UserDetails();
//			user.setUserName("Adrian " + i);
//			session.save(user);
//		}
		

		UserDetails u = (UserDetails)session.get(UserDetails.class, 3);
		u.setUserName("update User");
		session.update(u);
		
		
		UserDetails u2 = (UserDetails)session.get(UserDetails.class, 3);
		System.out.println("Updated user is : "+u2.getUserName());

		session.getTransaction().commit();
		session.close();
		sessionFactory.close();
	}
}
