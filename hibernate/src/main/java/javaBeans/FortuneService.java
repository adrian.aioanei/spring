package javaBeans;

public interface FortuneService {
	public String printMessage();

	default public String getMessage() {return "From interface";}
	default public void setMessage(String msg) {}
}
