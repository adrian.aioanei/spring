package javaBeans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {
	
	@Autowired
	@Qualifier("RESTFortuneService")
	private FortuneService fortuneService;
	
	@PostConstruct
	public void test() {
		System.out.println("Bean construction");
	}
	
	@PreDestroy
	public void testDestory() {
		System.out.println("Bean destruction !!!!!!!!!!");
	}
	
	public String printMessage() {
		return fortuneService.printMessage();
	}
	
	public String getMessage() {
		return fortuneService.getMessage();
	}
	
	public void setMessage(String msg) {
		fortuneService.setMessage(msg);
	}

}
