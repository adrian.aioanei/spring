package javaBeans;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnonationDemoApp {

	public static void main(String[] args) {
		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		
		SwimCoach theCoach = context.getBean("swimCoach", SwimCoach.class);
		System.out.println("Message before edit : " + theCoach.printMessage());
		
		//call new methods
		System.out.println("email " + theCoach.getEmail());
		System.out.println("team "  + theCoach.getTeam());
		

		context.close();
	}
}
