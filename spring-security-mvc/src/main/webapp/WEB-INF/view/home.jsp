<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>


<html>

<head>
<title>luv2code Company Home Page</title>
</head>

<body>
	<h2>luv2code Company Home Page</h2>
	<hr>

	Welcome to the luv2code company home page!

	<form:form action="${pageContext.request.contextPath}/logout"
		method="POST">
		<input type="submit" value="Logout">
	</form:form>


	<p>
		User:
		<security:authentication property="principal.username" />
		<br>
		<br> Rols:
		<security:authentication property="principal.authorities" />
	</p>

	<br>
	<h3>Availabele links</h3>
	<p>

		<security:authorize access="hasRole('MANAGER')">
			<a href="${pageContext.request.contextPath}/leaders">LeaderShip Meeting</a>
		</security:authorize>

		<security:authorize access="hasRole('ADMIN')">
			<a href="${pageContext.request.contextPath}/admin">Admin only</a>
		</security:authorize>

	</p>

</body>

</html>